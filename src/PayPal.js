import React, { useRef, useEffect } from "react";

export default function Paypal({ totalAmount }) { // Receive the totalAmount prop
  const paypal = useRef();

  useEffect(() => {
    window.paypal
      .Buttons({
        createOrder: (data, actions, err) => {
          return actions.order.create({
            intent: "CAPTURE",
            purchase_units: [
              {
                description: "table",
                amount: {
                  currency_code: "USD",
                  value: totalAmount, // Use the totalAmount prop here
                },
              },
            ],
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          console.log(order);
        },
        onError: (err) => {
          console.log(err);
        },
      })
      .render(paypal.current);
  }, [totalAmount]);

  return (
    <div>
      <div ref={paypal}></div>
    </div>
  );
}
