import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ShoppingCart } from "phosphor-react";
import "./navbar.css";
import { ShopContext } from "../context/shop-context";

export const Navbar = () => {
  const { cartItemCount } = useContext(ShopContext);

  return (
    <div className="navbar">
      <h1 className="logo">HK</h1>
      <div className="links">
        <Link to="/"> Shop </Link>
        <Link to="/contact"> Contact </Link>
        <Link to="/cart" className="cart-icon">
          <ShoppingCart size={32} />
          {cartItemCount > 0 && <div className="cart-counter">{cartItemCount}</div>}
        </Link>
      </div>
    </div>
  );
};
